const express = require('express');
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const Route = require('./routes')

const PORT = process.env.PORT || 8008
const app = express();
// const mongoConnection = require('./servicese/mongodb');
dotenv.config();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
Route(app);

app.listen(PORT, () =>{
  console.log('Server is up and running on port numner: ' + PORT)
});
