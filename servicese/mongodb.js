const mongoose = require('mongoose')
const url = process.env.DB_LOCALHOST || "mongodb+srv://tuonglam:dinovative@nc419-q70lr.gcp.mongodb.net/test?retryWrites=true&w=majority"  //'mongodb://localhost:27017/api'

mongoose.connect(url, { useNewUrlParser: true }).then(() =>{
  console.log(`Database is connected on URL: ${url}`)
}).catch(err =>{
  console.log(err)
  close()
});

module.exports = mongoose;

// const MongoClient = require(‘mongodb’).MongoClient;
// const uri = "mongodb+srv://PhatLuong:<password>@nc419-q70lr.gcp.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });