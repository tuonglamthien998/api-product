const Contact = require('../model').Contact


  const getList= (req, res) =>{
    Contact.find().then((Contact) =>{
      console.log(res)
      return res.status(200).json({
        status: true,
        message: 'getList Contact success',
        Contact: { Contact }
      })
    }).catch(err =>{
      console.log(err)
      return res.status(500).json({
        status: false,
        message: 'Something wrong Server',
        err : err
      })
    })
  };

  const create= async (req, res) =>{
    Contact.create(req.body).then(Contact =>{
      console.log(Contact)
      return res.status(200).json({
        status: true,
        message: 'create Contact success',
        Contact: { Contact }
      });
    }).catch(err =>{
      console.log(err);
      return res.status(500).json({
        status: false,
        message: "Can't create Contact. Something error with server",
        err: err
      })
    })
  };
  
  const findDetail = async (req, res) =>{
    let contact = await Contact.findById(req.params.id)
    if(!contact){
      return res.status(404).json({
        status: false,
        message: "Can't find anything with _id"
      })
    }
    return res.status(200).json({
      status: true,
      message: `Find Contact success. id: ${Contact._id}`,
      Contact: { contact }
    })
  }

  module.exports = {
    getList,
    create,
    findDetail
  }