const product = require('./product');
const contact = require('./contact');

module.exports = {
  product,
  contact
}