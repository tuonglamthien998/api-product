const Product = require('../model').Product


  const show= (req, res) =>{
    Product.find().then((product) =>{
      return res.status(200).json({
        status: true,
        message: 'getList product success',
        product: product
      })
    }).catch(err =>{
      console.log(err)
      return res.status(500).json({
        status: false,
        message: 'Something wrong Server',
        err : err
      })
    })
  };

  const stored= async (req, res) =>{
    Product.create(req.body).then(product =>{
      console.log(product)
      return res.status(200).json({
        status: true,
        message: 'create product success',
        product: product
      });
    }).catch(err =>{
      console.log(err);
      return res.status(500).json({
        status: false,
        message: "Can't create product. Something error with server",
        err: err
      })
    })
  };
  
  const findDetail = async (req, res) =>{
    let product = await Product.findById(req.params._id)
    if(!product){
      return res.status(404).json({
        status: false,
        message: "Can't find anything with _id"
      });
    }
    return res.status(200).json({
      status: true,
      message: `Find product success. id: ${product._id}`,
      product: product
    })
  }

  module.exports = {
    show,
    stored,
    findDetail
  }