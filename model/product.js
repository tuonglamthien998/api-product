const mongoose = require("../servicese/mongodb");
const Schema = mongoose.Schema;

const Product = new Schema(
	{
		name: String,
		imageUrl: String,
		quantity: Number,
		price: {
			type: Number,
			default: 0
		},
		description: String,
		status: {
			type: String,
			enum: ["PENDING", "ACTIVE", "LOCKED"],
			default: "PENDING"
		}
	},
	{
		collection: "Product",
		timestamps: true
	}
);

module.exports = mongoose.model('contact', Product);
