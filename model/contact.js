const mongoose = require('../servicese/mongodb');
const Schema = mongoose.Schema;

let contactSchema = new Schema({
  firstName: String,
  lastName: String, 
  imageUrl: String,
  summary: String,
  dateCreated: {
    type: Date,
    default: Date.now
  }
});
contactSchema.virtual('fullname').get( function() {
  return this.firstName + ' ' + this.lastName;
});

module.exports = mongoose.model('Contact', contactSchema)
