## install

```npm run dev```

## Start project development

```npm run dev```

## URL
```
GET localhost:8008/api/product
GET localhost:8008/api/product/:id
POST localhost:8008/api/product
```

## data
```
"price": Number
"status": "ACTIVE" || "PENDING" || "LOCKED", (String)
"name": String,
"imageUrl": String,
"quantibty": Number,
"description": String,
```