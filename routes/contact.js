const express = require('express');
const router = express.Router();
const contactCtrl = require('../controllers').contact

  router.get('/contact', contactCtrl.getList)
  router.get('/contact/:id', contactCtrl.findDetail)
  router.post('/contact', contactCtrl.create)

module.exports = router