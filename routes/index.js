const contact = require('./contact');
const product = require('./product');

module.exports = app =>{
  // RESHful
  app.use('/MVC', contact);
  // MVC
  app.use('/api', product);
}