const express = require("express");
const router = express.Router();
const productCtrl = require("../controllers").product;

router.get("/product", productCtrl.show);
router.post("/product", productCtrl.stored);
router.get("/product/:_id", productCtrl.findDetail);

module.exports = router;
